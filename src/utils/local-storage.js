export default {
  key: (n) => window.localStorage.key(n),

  get: (key) => {
    const jsonValue = window.localStorage.getItem(key);
    return JSON.parse(jsonValue);
  },

  set: (key, value) => {
    const jsonValue = JSON.stringify(value);
    window.localStorage.setItem(key, jsonValue);
  },

  remove: (key) => {
    window.localStorage.removeItem(key);
  },

  clear: () => {
    window.localStorage.clear();
  }
};
