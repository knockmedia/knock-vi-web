export const actions = {
  nuxtServerInit: async ({ dispatch }, context) => {
    await Promise.all([
      dispatch('menus/fetchBySlug', { slug: 'main-menu', context })
    ]);
  }
};
