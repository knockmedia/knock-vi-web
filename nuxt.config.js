/* eslint-disable no-await-in-loop, no-plusplus  */

import path from 'path';

import axios from 'axios';

const sleep = (ms) => new Promise((resolve) => { setInterval(resolve, ms); });

export default {
  srcDir: path.resolve(__dirname, 'src'),

  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Knockmedia',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { hid: 'og:title', property: 'og:title', content: '' },
      { hid: 'description', name: 'og:description', content: '' },
      { hid: 'og:description', property: 'description', content: '' },
      { hid: 'og:locale', property: 'locale', content: 'en_US' },
      { hid: 'og:site_name', property: 'site_name', content: 'Knockmedia' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        hid: 'hotjar-tracking',
        innerHTML: `
          (function(h,o,t,j,a,r){
              h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
              h._hjSettings={hjid:3311561,hjsv:6};
              a=o.getElementsByTagName('head')[0];
              r=o.createElement('script');r.async=1;
              r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
              a.appendChild(r);
          })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        `
      },
      {
        async: true,
        src: 'https://www.googletagmanager.com/gtag/js?id=G-9MMQRJGJFV'
      },
      {
        hid: 'google-analytics',
        innerHTML: `window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-9MMQRJGJFV');`
      }
    ],
    __dangerouslyDisableSanitizersByTagID: {
      'hotjar-tracking': ['innerHTML'],
      'google-analytics': ['innerHTML']
    }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'normalize.css',
    '@/assets/css/main.scss'
  ],

  styleResources: {
    scss: [
      '@/assets/css/variables.scss'
    ]
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/modal' },
    { src: '@/plugins/wp-json' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/google-analytics',
    '@nuxt/postcss8',
    '@nuxtjs/style-resources',
    '@nuxtjs/fontawesome'
  ],

  fontawesome: {
    icons: {
      solid: ['faArrowLeftLong', 'faArrowRightLong', 'faMagnifyingGlass'],
      brands: ['faTwitter', 'faLinkedinIn', 'faXTwitter', 'faBluesky']
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, { isClient }) {
      if (isClient) {
        config.devtool = 'source-map';
      }
    }
  },

  generate: {
    fallback: '404.html',

    interval: 100,

    concurrency: 100,

    routes: async () => {
      const POSTS_PER_PAGE = 50;

      try {
        const request = axios.create({ baseURL: `${process.env.WP_API_URL}/wp-json` });

        // Get posts

        const posts = [];
        let page = 1;
        let totalPages = 1;

        while (page <= totalPages) {
          const time = Date.now();

          console.log(`Fetching posts ${(page - 1) * POSTS_PER_PAGE + 1} - ${page * POSTS_PER_PAGE}...`);

          const response = await request({
            method: 'GET',
            url: 'wp/v2/posts',
            params: {
              per_page: POSTS_PER_PAGE,
              page,
              acf_format: 'standard',
              _embed: true
            }
          });

          response.data.forEach((post) => {
            posts.push({
              ...post,
              thumbnail: post?._embedded?.['wp:featuredmedia']?.[0],
              author: post?.acf.author
            });
          });

          totalPages = parseInt(response.headers['x-wp-totalpages']);

          page++;

          console.log(`Posts fetched. Total time ${((Date.now() - time) / 1000).toFixed(2)}s`);

          await sleep(1000);
        }

        // Get category names

        const response = await request({
          method: 'GET',
          url: '/wp/v2/categories',
          params: {
            per_page: 100
          }
        });

        const catNames = {};
        let categories = response.data;

        categories = categories.filter((cat) => cat.count !== 0);

        categories.some((cat, id) => cat.id === 1 && categories.unshift(
          categories.splice(id, 1)[0]
        ));

        categories.forEach((category) => {
          catNames[category.id] = category.name;
        });

        // Create route objects with payloads

        const routes = posts.map((post) => ({
          route: post.link.replace(/\/$/, ''),
          payload: {
            post: {
              ...post,
              categories: post.categories.filter((cat) => cat !== 1)
            },
            catNames,
            posts: posts.filter((p) => p.id !== post.id).slice(0, 2)
          }
        }));

        // Get pages
        page = 1;
        totalPages = 1;

        while (page <= totalPages) {
          const time = Date.now();

          console.log(`Fetching pages ${(page - 1) * POSTS_PER_PAGE + 1} - ${page * POSTS_PER_PAGE}...`);

          const response = await request({
            method: 'GET',
            url: 'wp/v2/pages',
            params: {
              per_page: POSTS_PER_PAGE,
              page,
              acf_format: 'standard'
            }
          });

          response.data.forEach((page) => {
            routes.push({
              route: page.link.replace(/\/$/, ''),
              page
            });
          });

          totalPages = parseInt(response.headers['x-wp-totalpages']);

          page++;

          console.log(`Pages fetched. Total time ${((Date.now() - time) / 1000).toFixed(2)}s`);

          await sleep(1000);
        }

        return routes;
      }
      catch (err) {
        console.error(err.response);
        throw err;
      }
    }
  },

  publicRuntimeConfig: {
    wpApiUrl: process.env.WP_API_URL || 'http://localhost:1337',
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID
    }
  }
};
