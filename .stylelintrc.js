module.exports = {
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-recommended-vue'
  ],

  plugins: [
    'stylelint-no-unsupported-browser-features'
  ],

  rules: {
    'no-descending-specificity': null,
    'no-empty-source': null,
    'string-quotes': 'single'
  }
}
