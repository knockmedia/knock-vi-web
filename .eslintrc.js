const path = require('path');

module.exports = {
  root: true,

  env: {
    browser: true,
    node: true
  },

  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false
  },

  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
    'plugin:nuxt/recommended',
    'plugin:vuejs-accessibility/recommended'
  ],

  plugins: [
  ],

  settings: {
    'import/core-modules': [
      'vue'
    ],
    'import/internal-regex': '^(@|~)/',
    'import/resolver': {
      alias: {
        map: [
          ['@', path.resolve(__dirname, 'src')],
          ['~', path.resolve(__dirname, 'src')],
          ['@@', path.resolve(__dirname)],
          ['~~', path.resolve(__dirname)]
        ]
      }
    }
  },

  rules: {
    'brace-style': ['error', 'stroustrup', { allowSingleLine: true }],
    'comma-dangle': ['error', 'never'],
    'no-console': ['warn', { allow: ['error'] }],
    'no-underscore-dangle': 'off',
    'import/order': ['error', {
      'groups': ['builtin', 'external', 'internal', ['parent', 'sibling'], 'index'],
      'newlines-between': 'always',
      'alphabetize': { order: 'asc', caseInsensitive: true }
    }],
    'import/prefer-default-export': 'off',
    'max-len': ['error', 120, 2, {
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true
    }],
    'no-param-reassign': ['error', { props: false }],
    'no-shadow': 'off',
    'no-unused-vars': ['error', { args: 'none' }],
    'prefer-destructuring': ['error', { array: false }],
    'quote-props': ['error', 'consistent-as-needed'],
    'radix': ['error', 'as-needed'],
    'vue/max-len': ['error', {
      code: 120,
      tabWidth: 2,
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
      ignoreHTMLAttributeValues: true,
      ignoreHTMLTextContents: true
    }],
    'vue/no-v-html': 'off'
  },

  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'max-len': 'off' // Handled by vue/max-len
      }
    }
  ]
};
